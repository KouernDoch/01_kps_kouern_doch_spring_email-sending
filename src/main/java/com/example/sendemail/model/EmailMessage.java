package com.example.sendemail.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailMessage {
    private  String toEmail;
    private String senderName;
    private  String subject;
    private  String message;
}
