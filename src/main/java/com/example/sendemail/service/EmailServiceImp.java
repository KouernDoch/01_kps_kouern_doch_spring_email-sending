package com.example.sendemail.service;

import com.example.sendemail.model.EmailMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
@Service
public class EmailServiceImp implements EmailService {
    @Autowired
    private JavaMailSender javaMailSender;
    @Value("${spring.mail.username}") private String sender;

    public String sendEmail(EmailMessage emailMessage) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(sender);
            mailMessage.setTo(emailMessage.getToEmail());
            mailMessage.setText("Dear Sir/Madam ,"+"\n\n"+emailMessage.getMessage()+"\n\n"+"Yours Truly"+"\n\n"+emailMessage.getSenderName());
            mailMessage.setSubject(emailMessage.getSubject());
            javaMailSender.send(mailMessage);
            return "Successfully";
        }
        catch (Exception e) {
            return "Error while Sending Mail";
        }
    }
}
