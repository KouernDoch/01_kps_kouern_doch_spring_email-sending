package com.example.sendemail.service;

import com.example.sendemail.model.EmailMessage;
import org.springframework.stereotype.Service;

@Service
public interface EmailService {
    String sendEmail(EmailMessage emailMessage);
}
