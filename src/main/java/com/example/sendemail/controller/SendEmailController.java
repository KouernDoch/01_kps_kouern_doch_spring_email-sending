package com.example.sendemail.controller;

import com.example.sendemail.model.EmailMessage;
import com.example.sendemail.service.EmailService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class SendEmailController {
   private final EmailService emailService;
    public SendEmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping("Homepage")
    public ModelAndView mainManu() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Index");
        return modelAndView;
    }
    @GetMapping("/myEmail")
    public ModelAndView getFormSendEmail() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("emailMessage", new EmailMessage());
        modelAndView.setViewName("/myEmail");
        return modelAndView;
    }
    @PostMapping("/api/v1/sendEmail")
    public ModelAndView sendEmail(@ModelAttribute EmailMessage emailMessage) {
        String response = emailService.sendEmail(emailMessage);
        ModelAndView modelAndView = new ModelAndView();
        if (response == "Successfully"){
            modelAndView.setViewName("redirect:/Homepage");
        } else {
            modelAndView.setViewName("redirect:/error");
        }
        return modelAndView;
    }
}
